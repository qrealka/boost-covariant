#ifdef compile_instructions
(echo "#include\""$0"\"" > $0x.cpp) && echo $0x.cpp && time c++ -std=c++2a -Wfatal-errors -D_TEST_BOOST_COVARIANT_OVERLOAD $0x.cpp -o $0x.cpp.x -lstdc++fs && ./$0x.cpp.x $@ && rm -f $0x.cpp $0x.cpp.x; exit;
#endif

#ifndef BOOST_COVARIANT_OVERLOAD_HPP
#define BOOST_COVARIANT_OVERLOAD_HPP

#include<utility>
#include<variant>

namespace boost{

template<class... Fs> 
struct overload : Fs...{
	overload(Fs... fs) : Fs(fs)...{}
	using Fs::operator()...;
};
template<class... Fs> overload(Fs... f) -> overload<Fs...>;

template<class... Fs>
struct regular_overload{
	overload<Fs...> o_;
	regular_overload(Fs... fs) : o_{fs...}{}
	template<class... Ts, typename = decltype(o_(std::declval<Ts>()...))>
	decltype(auto) operator()(Ts&&... ts){
		if constexpr(std::is_same<decltype(o_(std::forward<Ts>(ts)...)), void>::value){
			o_(std::forward<Ts>(ts)...);
			return std::monostate{};
		}else{
			return o_(std::forward<Ts>(ts)...);
		}
	}
};


template<class Ret, class... Fs>
struct static_overload : overload<Fs...>{
//	using overload<Fs...>::overload;
	static_overload(Fs... fs) : overload<Fs...>{fs...}{}
	template<class... Ts> Ret operator()(Ts&&... ts){
		return overload<Fs...>::operator()(std::forward<Ts>(ts)...);
	}
};

template<class Ret, class... Fs>
auto make_static_overload(Fs... fs){return static_overload<Ret, Fs...>(fs...);}

#if 0
template<class F>
struct overload<F> : F{
	overload(F f) : F(std::move(f)){};
	using F::operator();
};
template<class F0, class... Fs> 
struct overload<F0, Fs...> : F0, overload<Fs...>{
	overload(F0 f0, Fs... fs) : F0(std::move(f0)), overload<Fs...>(std::move(fs)...){}
	using F0::operator();
	using overload<Fs...>::operator();
};
#endif

}

#ifdef _TEST_BOOST_COVARIANT_OVERLOAD
#include<cassert>
#include<iostream>
#include<variant>

using std::cout;

int main(){

	using std::variant;
	using boost::overload;
	using boost::make_static_overload;
	using boost::regular_overload;

	variant<double, int> v1 = 3.14;
	variant<double, int> v2 = 5;

	{
		auto o1 = overload{
			[](double d){return 2*d;}, 
			[](int    i){return 2*i;},
		};
		assert( o1(3.14) == 6.28 );
		assert( o1(5) == 10 );

		auto o2 = overload{
			[](auto n){return 2*n;}
		};
		assert( o2(3.14) == 6.28 );
	}
	{
		auto o1 = make_static_overload<int>(
			[](double d){return 2*d;}, 
			[](int    i){return 2*i;},
			[](std::string s){return s;} //lazy, only on instantiation will fail
		);
		auto i = o1(5.4);
		static_assert(std::is_same<decltype(i), int>{} , "");
		assert(i == 10);
	}
	{
		auto d = std::visit(boost::overload{
			[](double d){return 2.*d;        }, 
			[](int    i){return double(2.*i);},
		}, v1);
		assert( d == 6.28 );
	}
	{
		auto d = std::visit(overload{
			[](double d1, double d2){return d1 + d2;        }, 
			[](double d1, int    i2){return d1 + i2;        },
			[](int    i1, double d2){return i1 + d2;        },
			[](int    i1, int    i2){return double(i1 + i2);}
		}, v1, v2);
		assert( d == 8.14 );
	}
	{
		auto r = regular_overload(
			[](double d){return 8.;},
			[](int){}
		)(3);
		static_assert(std::is_same<decltype(r), std::monostate>{} , "");
	}
}
#endif
#endif

