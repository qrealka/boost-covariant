#ifdef compile_instructions
(echo "#include\""$0"\"" > $0x.cpp) && echo $0x.cpp && time c++ -std=c++17 -Wfatal-errors -D_TEST_BOOST_COVARIANT_COVARIANT $0x.cpp -o $0x.cpp.x -lstdc++fs && ./$0x.cpp.x $@ && rm -f $0x.cpp $0x.cpp.x; exit;
#endif

#ifndef BOOST_COVARIANT_COVARIANT_HPP
#define BOOST_COVARIANT_COVARIANT_HPP

#include "../covariant/overload.hpp"

#include "../covariant/detail/variant_of_set.hpp"
#include "../covariant/detail/result_of_set.hpp"
#include "../covariant/detail/variant_types_list.hpp"

namespace boost{

template<class... Fs>
struct covariant : overload<Fs...>{
	covariant(Fs... fs) : overload<Fs...>(fs...){}
	template<class... Ts, typename = decltype(overload<Fs...>::operator()(std::declval<Ts>()...))> 
	decltype(auto) call(Ts&&... ts) const{
		if constexpr(std::is_same<decltype(overload<Fs...>::operator()(std::forward<Ts>(ts)...)), void>::value){
			overload<Fs...>::operator()(std::forward<Ts>(ts)...);
			return std::monostate{};
		}else{
			return overload<Fs...>::operator()(std::forward<Ts>(ts)...);
		}
	}
	template<class... Ts, class Ret = detail::variant_of_set_t<detail::results_of_set_t<overload<Fs...> const&, detail::variant_types_list_t<std::variant<Ts...>>>>>
	Ret operator()(std::variant<Ts...> const& v){
		return std::visit([&](auto&& e)->Ret{return call(e);}, v);
	}
};
template<class... Fs> covariant(Fs... f) -> covariant<Fs...>;

}

#ifdef _TEST_BOOST_COVARIANT_COVARIANT

#include<cassert>
#include<iostream>
#include<variant>

#include<boost/variant.hpp>
#include<complex>
using std::cout;

int main(){

	using std::variant;
	using std::visit;
	using boost::overload;
	using boost::covariant;

	variant<double, int, std::string, char> v1 = 3.14;

	auto next_overload = overload(
		[](int a){return a + 1;},
		[](double a){return a + 1;},
		[](char){return int(-1);}
	);

	assert( next_overload(2) == 3 );
	assert( next_overload(3.14) == 3.14 + 1 );
	assert( next_overload('a') == -1 );
//	static_assert( std::is_same<decltype(next_overload(std::string{})), void>{}, ""); 

//	auto r = std::visit(next_overload, v1); // error! next_overload, not a valid visitor

	auto next_overload_void = overload(
		[](int a){return a + 1;},
		[](double a){return a + 1;},
		[](char){return int(-1);},
		[](std::string)->void{}
	);

	assert( next_overload(2) == 3 );
	static_assert( std::is_same<decltype(next_overload_void(std::string{})), void>{}, ""); 

	auto next_covariant = covariant(
		[](int a){return a + 1;},
		[](double a){return a + 1.0;},
		[](char){return int(-1);},
		[](std::string){return int(-1);}
	);
	assert( next_covariant(2) == 3 );
	assert( next_covariant(4.1) == 4.1 + 1.0 );
	assert( next_covariant('a') == -1 );
	assert( next_covariant(std::string{}) == -1 );
	
	auto v2 = next_covariant(v1);
	static_assert( std::is_same<decltype(v2), std::variant<double, int>>{}, "");
	assert( std::get<double>(v2) == 3.14 + 1.0 );
	assert( std::get_if<int>(&v2) == nullptr );

	auto next_covariant_void = covariant(
		[](int a){return a + 1;},
		[](double a){return a + 1.0;},
		[](char){return int(-1);},
		[](std::string)->void{}
	);
	assert( next_covariant_void(2) == 3 );
	assert( next_covariant_void(4.1) == 4.1 + 1.0 );
	assert( next_covariant_void('a') == -1 );
	static_assert( std::is_same<decltype(next_covariant_void(std::string{})), void>{} );
	
	auto v3 = next_covariant_void(v1);
	static_assert( std::is_same<decltype(v3), std::variant<double, int, std::monostate>>{}, "");

	assert( std::get<double>(v3) == 3.14 + 1.0 );
	assert( std::get_if<int>(&v3) == nullptr );
	assert( std::get_if<std::monostate>(&v3) == nullptr );
	std::visit(overload(
		[](auto const& v){std::cout << v;}, 
		[](std::monostate){std::cout << "mono";}
	), v3);

	auto next_covariant_fallback = covariant(
		[](int a){return a + 1;},
		[](double a){return a + 1.0;},
		[](...){return int(-1);}
	);
	assert( next_covariant_fallback(2) == 3 );
	assert( next_covariant_fallback(4.1) == 4.1 + 1.0 );

	auto v4 = next_covariant_fallback(v1);
	assert( std::get<double>(v4) == 3.14 + 1.0 );
	assert( std::get_if<int>(&v3) == nullptr );

	auto v5 = next_covariant_fallback(std::variant<int, double, std::string>(std::string("hello")));
	assert( std::get<int>(v5) == -1 );

	{
		using complex = std::complex<double>;
		variant<double, complex> n = 4.; //complex{2.,2.};
		variant<double> v2 = covariant([](auto&& n){return std::abs(n);})(n);
		auto d = std::get<0>(v2);
	}
}
#endif
#endif

