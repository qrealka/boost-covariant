#ifdef compile_instructions
(echo "#include\""$0"\"" > $0x.cpp) && echo $0x.cpp && time c++ -std=c++17 -Wfatal-errors -D_TEST_BOOST_COVARIANT_DETAIL_RESULT_OF_SET $0x.cpp -o $0x.cpp.x && ./$0x.cpp.x $@ && rm -f $0x.cpp $0x.cpp.x; exit;
#endif

#ifndef BOOST_COVARIANT_DETAIL_RESULT_OF_SET_HPP
#define BOOST_COVARIANT_DETAIL_RESULT_OF_SET_HPP

#include<variant>
#include<boost/mpl/fold.hpp>
#include<boost/mpl/set.hpp>
#include<boost/mpl/transform_view.hpp>

namespace boost{
namespace detail{

namespace bmp = boost::mpl;

template<class F, class T>
struct apply{
	using type = decltype(std::declval<F>()(std::declval<T>()));
};

/*template<class, class> struct mpl_variant_push_back;
template<class... Vs, class T>
struct mpl_variant_push_back<std::variant<Vs...>, T>{
	using type = std::variant<Vs..., T>;
};*/

template<class O, class Seq> 
using results_of_set_t = 
	typename bmp::fold<
		typename bmp::transform_view<
			Seq,
			apply<O, bmp::_1>
		>::type,
		bmp::set<>,
		bmp::insert<bmp::_1, bmp::_2>
	>::type
;

}}

#ifdef _TEST_BOOST_COVARIANT_DETAIL_RESULT_OF_SET

#include<boost/mpl/list.hpp>
#include<boost/mpl/size.hpp>

#include<string>

struct F{
	int operator()(int i);//{return i;}
	double operator()(double d);//{return d;}
	double operator()(std::string s);//{return 3.14;}
};

int main(){

	namespace bmp = boost::mpl;
	using a_list = bmp::list<double, int, std::string>;
	using a_set = boost::detail::results_of_set_t<F, a_list>;
	static_assert( bmp::size<a_set>::value ==  2 , "");

}
#endif
#endif

