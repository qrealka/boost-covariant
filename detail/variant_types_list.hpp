#ifdef compile_instructions
(echo "#include\""$0"\"" > $0x.cpp) && echo $0x.cpp && time c++ -std=c++17 -Wfatal-errors -D_TEST_BOOST_COVARIANT_DETAIL_VARIANT_TYPES_LIST $0x.cpp -o $0x.cpp.x -lstdc++fs && ./$0x.cpp.x $@ && rm -f $0x.cpp $0x.cpp.x; exit;
#endif

#ifndef BOOST_COVARIANT_DETAIL_VARIANT_TYPES_LIST_HPP
#define BOOST_COVARIANT_DETAIL_VARIANT_TYPES_LIST_HPP

#include<variant>

#include<boost/mpl/list.hpp>

namespace boost{
namespace detail{

namespace bmp = boost::mpl;

template<class T> struct variant_types_list
//{
//	using type = bmp::list<T>;
//}
;

template<class... Ts>
struct variant_types_list<std::variant<Ts...>>{
	using type = bmp::list<Ts...>;
};

template<class T> using variant_types_list_t = typename variant_types_list<T>::type;

}}

#ifdef _TEST_BOOST_COVARIANT_DETAIL_VARIANT_TYPES_LIST

#include<boost/mpl/front.hpp>

int main(){
	namespace bmp = boost::mpl;
	{
		using a_variant = std::variant<double, int>;
		using a_list = boost::detail::variant_types_list_t<a_variant>;
		static_assert( std::is_same<bmp::front<a_list>::type, double>{}, "" );
	}
/*	{
		using T = double;
		using a_list = boost::detail::variant_types_list_t<T>;
		static_assert( std::is_same<bmp::front<a_list>::type, double>{}, "" );
	}*/

}
#endif
#endif

