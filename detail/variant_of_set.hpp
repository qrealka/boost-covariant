#ifdef compile_instructions
(echo "#include\""$0"\"" > $0x.cpp) && echo $0x.cpp && time c++ -std=c++17 -Wfatal-errors -D_TEST_BOOST_COVARIANT_DETAIL_VARIANT_OF_SET $0x.cpp -o $0x.cpp.x && ./$0x.cpp.x $@ && rm -f $0x.cpp $0x.cpp.x; exit;
#endif

#ifndef BOOST_COVARIANT_DETAIL_VARIANT_OF_SET_HPP
#define BOOST_COVARIANT_DETAIL_VARIANT_OF_SET_HPP

#include<type_traits>
#include<variant>
#include<boost/mpl/fold.hpp>

namespace boost{
namespace detail{

namespace bmp = boost::mpl;

template<class, class> struct variant_push_back;
template<class... Vs, class T>
struct variant_push_back<std::variant<Vs...>, T>{
	using type = std::variant<
		std::conditional_t<
			std::is_same<T, void>::value
			, std::monostate 
			, T
		>,
		Vs...
	>;
};

template<class Set> 
using variant_of_set_t = 
	typename bmp::fold<
		Set,
		std::variant<>,
		variant_push_back<bmp::_1, bmp::_2>
	>::type;
;

}}

#ifdef _TEST_BOOST_COVARIANT_DETAIL_VARIANT_OF_SET

#include<boost/mpl/set.hpp>
#include<boost/mpl/set/set0.hpp>

#include<boost/mpl/vector.hpp>
#include<string>

int main(){

	namespace bmp = boost::mpl;
	{
		using a_set = bmp::set<double, int, std::string, void>;
		using a_variant = boost::detail::variant_of_set_t<a_set>;
		static_assert( 
			std::is_same<
				std::variant<double, int, std::string, std::monostate>, 
				a_variant
			>::value, 
			""
		);
	}
	{
		using a_set = bmp::set<int, double, std::string, void>;
		using a_variant = boost::detail::variant_of_set_t<a_set>;
		static_assert( 
			std::is_same<
				std::variant<int, double, std::string, std::monostate>, 
				a_variant
			>::value, 
			""
		);
	}

}
#endif
#endif

